@extends('app')

@section('title', isset($Business) ? $Business->name : 'Create new Business')

@section('content')
@if (isset($Business))
<div class="modal fade" id="sureDelete" tabindex="-1" role="dialog" aria-labelledby="sureDeleteLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="sureDeleteLabel">Delete Business?</h5>
      </div>
      <div class="modal-body">
        <p class="lead text-warning"> Are you sure to delete this? </p>
        <p class="">This process <strong>can't be undone.</strong></p>
      </div>
      <div class="modal-footer">

        <form action="{{ route('businesses.destroy', $Business->id) }}" method="POST">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <input type="hidden" name="_method" value="DELETE">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        <button type="submit" class="btn btn-danger">Yes, Delete</button>
        </form>

      </div>
    </div>
  </div>
</div>
@endif
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
          <p class="panel-title">Business</p>
        </div>

				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

				<form class="form-horizontal" role="form" method="POST" action="{{ isset($Business) ? route('businesses.update', $Business->id) : route('businesses.store') }}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
               @if (isset($Business))
               <input type="hidden" name="_method" value="PUT">
               @endif

               <div class="form-group">
                  <label class="col-md-4 control-label">Category</label>
                  <div class="col-md-6">
                     <select name="category" class="form-control">
                        <option value="-1">Select a Category</option>
                        @foreach ($Categories as $Category)
                        <option value="{{ $Category->id }}" {{ old('category', isset($Business) ? $Business->category_id : '0') == $Category->id ? 'selected' : '' }}>{{ $Category->name }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>

               <div class="form-group">
                  <label class="col-md-4 control-label">Name</label>
                  <div class="col-md-6">
                     <input type="text" name="name" class="form-control" value="{{ old('name', isset($Business) ? $Business->name : '') }}">
                  </div>
               </div>

					<div class="form-group">
						<label class="col-md-4 control-label">Description</label>
						<div class="col-md-6">
							<textarea name="description" class="form-control" rows="4">{{ old('description', isset($Business) ? $Business->description : '') }}</textarea>
						</div>
					</div>

          <div class="form-group">
            <label class="col-md-4 control-label">Picture</label>
            <div class="col-md-6">
              <input name="photo" value="Choose..." accept="image/*" type="file" class="form-control btn-xs">
            </div>
          </div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</div>
				</form>

        @if (isset($Business))
          @if ($Business->photo_uri)
          <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <div class="well well-sm">
              <img src="/{{ $Business->photo_uri }}" class="img-responsive">
            </div>
          </div>
          @endif
        @endif

				</div>

				@if (isset($Business))
				<div class="panel-footer text-right">
					<button class="btn btn-danger" data-toggle="modal" data-target="#sureDelete">Delete</button>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection
