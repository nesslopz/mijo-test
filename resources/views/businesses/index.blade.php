@extends('app')
@section('title', 'All Businesses')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading clearfix">
               <span class="panel-title">All Business</span>
               <span class="pull-right">
                  <a href="{{ route('businesses.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</a>
               </span>
            </div>

            <div class="panel-body">

               <div class="row">

                  @foreach($Businesses as $Business)

                  <div class="col-sm-6 col-md-4">
                     <div class="panel panel-default">
                        <div class="panel-footer">
                           <p class="h2">{{$Business->name}}</p>
                           <p class="small">{{ $Business->description }}</p>
                           <p class="text-right">
                              @if ($Business->photo_uri)
                              <span class="lead pull-left text-muted">
                                 <span class="glyphicon glyphicon-picture"></span>
                              </span>
                              @endif
                              <a href="{{ route('businesses.edit', $Business->id) }}" class="btn btn-default">Edit</a>
                           </p>
                        </div>
                     </div>
                  </div>

                  @endforeach

               </div>

            </div>
         </div>
      </div>
   </div>
</div>
@endsection
