@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Welcome</div>

				<div class="panel-body">
					<a href="{{ route('categories.index') }}" class="btn btn-primary btn-lg">Categories</a>
					<a href="{{ route('businesses.index') }}" class="btn btn-primary btn-lg">Business</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
