@extends('app')
@section('title', 'All Categories')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading clearfix">
               <span class="panel-title">All Categories</span>
               <span class="pull-right">
                  <a href="{{ route('categories.create') }}" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</a>
               </span>
            </div>

            <div class="panel-body">

               <div class="row">

                  @foreach($Categories as $Category)

                  <div class="col-sm-6 col-md-4">
                     <div class="panel panel-default">
                        <div class="panel-footer">
                           <p class="h2">{{$Category->name}}</p>
                           <p class="small">{{ $Category->description }}</p>
                           <p class="text-right">
                              <a href="{{ route('categories.edit', $Category->id) }}" class="btn btn-default">Edit</a>
                           </p>
                        </div>
                     </div>
                  </div>

                  @endforeach

               </div>

            </div>
         </div>
      </div>
   </div>
</div>
@endsection
