<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#DF691A">
	<title>BussinessList</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">BussinessList</a>
			</div>
		</div>
	</nav>

	<div class="container-fluid">

		<div class="panel panel-default">

			<div class="panel-heading">
				Businesses
			</div>

			<div class="panel-body">

				<div class="row">

					@foreach ($Businesses as $Business)
					<div class="col-sm-6 col-md-4">
						<div class="panel panel-default">
							<div class="panel-footer">
								<p class="lead">{{ $Business->name }}</p>
								<p class="small">{{ $Business->description }}</p>
								<p class="text-right">
									<a href="{{ url($Business->slug) }}" class="btn btn-primary"> View </a>
								</p>
							</div>
						</div>
					</div>
					@endforeach

				</div>

			</div>

		</div>

	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>