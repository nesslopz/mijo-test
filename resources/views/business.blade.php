@extends('app')

@section('title', $Business->name)
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-10 col-md-offset-1">
         <div class="panel panel-default">
            <div class="panel-heading"><h2>{{ $Business->name }}</h2></div>

            <div class="panel-body">
               <div class="row">

                  @if ($Business->photo_uri)
                  <div class="col-md-6">
                     <img src="/{{ $Business->photo_uri }}" class="img-responsive">
                  </div>
                  @endif

                  <div class="{{ $Business->photo_uri ? 'col-md-6' : 'col-md-12' }}">
                     <p class="lead">
                        <strong>Category</strong> <br>
                        {{ $Business->category->name }}
                     </p>
                     <p class="lead">
                        <strong>Description</strong> <br>
                        {{ $Business->description }}
                     </p>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
