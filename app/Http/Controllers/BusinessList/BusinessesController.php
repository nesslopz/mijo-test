<?php namespace App\Http\Controllers\BusinessList;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Business;
use Validator;

class BusinessesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$Businesses = Business::all();
		return view('businesses.index', compact('Businesses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$Categories = Category::all();
		return view('businesses.form', compact('Categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules());

		if($validator->fails())
		{
			return redirect()->route('businesses.create')
						 ->withErrors($validator)
						 ->withInput();
		}

		$Business              = new Business;
		$Business->category_id = $request->get('category');
		$Business->name        = $request->get('name');
		$Business->description = $request->get('description');

		// Picture
		if ($request->hasFile('photo'))
		   {
				$image = $request->file('photo');
				$ext    = $image->getClientOriginalExtension();
				$image->move('static/images/businesses/', $Business->slug . '.' .$ext);

				$Business->photo_uri   = 'static/images/businesses/' . $Business->slug . '.' .$ext;
		   }
		else
			$Business->photo_uri = null;

		$Business->slug        = str_slug($request->get('name'));
		$Business->save();

		return redirect()->route('businesses.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$Business   = Business::find($id);
		$Categories = Category::all();

		if ($Business)
			return view('businesses.form', compact('Business', 'Categories'));

		abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules());

		if($validator->fails())
		{
			return redirect()->route('businesses.edit', $id)
						 ->withErrors($validator)
						 ->withInput();
		}

		$Business              = Business::find($id);
		$Business->category_id = $request->get('category');
		$Business->name        = $request->get('name');
		$Business->description = $request->get('description');
		$Business->slug        = str_slug($request->get('name'));

		// Picture
		if ($request->hasFile('photo'))
		   {
				$image = $request->file('photo');
				$ext    = $image->getClientOriginalExtension();
				$image->move('static/images/businesses/', $Business->slug . '.' .$ext);

				$Business->photo_uri   = 'static/images/businesses/' . $Business->slug . '.' .$ext;
		   }

		$Business->save();

		return redirect()->route('businesses.index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Business::find($id)->delete();
		return redirect()->route('businesses.index');
	}

	private function rules()
	{
		return [
			'description' => 'required',
			'name'        => 'required',
		];
	}
}
