<?php namespace App\Http\Controllers\BusinessList;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Category;
use Validator;

class CategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$Categories = Category::all();
		return view('categories.index', compact('Categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('categories.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules());

		if($validator->fails())
		{
			return redirect()->route('categories.create')
						 ->withErrors($validator)
						 ->withInput();
		}

		$Category              = new Category;
		$Category->name        = $request->get('name');
		$Category->description = $request->get('description');
		// $Category->icon     = $request->get('icon');
		$Category->slug        = str_slug($request->get('name'));
		$Category->save();

		return redirect()->route('categories.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$Category = Category::find($id);

		if ($Category)
			return view('categories.form', compact('Category'));

		abort(404);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules());

		if($validator->fails())
		{
			return redirect()->route('categories.edit', $id)
						 ->withErrors($validator)
						 ->withInput();
		}

		$Category              = Category::find($id);
		$Category->name        = $request->get('name');
		$Category->description = $request->get('description');
		// $Category->icon     = $request->get('icon');
		$Category->slug        = str_slug($request->get('name'));
		$Category->save();

		return redirect()->route('categories.index');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Category::find($id)->delete();
		return redirect()->route('categories.index');
	}

	private function rules()
	{
		return [
			'description' => 'required',
			'name'        => 'required',
		];
	}

}
