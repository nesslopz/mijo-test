<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->insert(array(
          'name'       => 'Admin',
          'email'      => 'admin@businesslist.com',
          'password'   => bcrypt('secret'),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ));
    }
}